#!/bin/sh

echo "Script convert Obsidian md-file to doc"
echo "and use pandoc and libreoffice (soffice)"
echo ""

if [ -z "$1" ]; then
	echo 'Error! The argument must be: filename without ".md"'
	exit
fi

pandoc $FNAME.md -o $FNAME.docx --from markdown+yaml_metadata_block+raw_html --top-level-division=chapter
soffice --convert-to doc --outdir ./ $FNAME.docx

echo 'Done'
