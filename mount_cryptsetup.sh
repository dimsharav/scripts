#!/bin/sh
#
# Mount truecrypt-volume with cryptsetup
#
# Need root privilegies and change variables

userid=1000
volume=/tmp/volume
mountpoint=/mnt

cryptsetup --type tcrypt open $volume tc-volume
mount -o umask=0077,uid=$userid /dev/mapper/tc-volume $mountpoint 
