#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
File: moodle_logins.py
Author: Дмитрий Шаравьев
Description:
Создание csv-файла с набором полей для импорта пользователей в Moodle
Формат полей:
http://docs.moodle.org/38/en/Upload_users#File_formats_for_upload_users_file

Зависимости:
Python 3
pip install pwgen

Использование:
    logins.py FILE
    FILE     - файл со списком строк формата:
    Фамилия Имя Отчество email
    Последнее поле - не обязательное
'''

from optparse import OptionParser
import pwgen


def translit(s):
    """translit string 's' from russian to english"""
    dic = {'а': 'a',
           'б': 'b',
           'в': 'v',
           'г': 'g',
           'д': 'd',
           'е': 'e',
           'ё': 'yo',
           'ж': 'zh',
           'з': 'z',
           'и': 'i',
           'й': 'i',
           'к': 'k',
           'л': 'l',
           'м': 'm',
           'н': 'n',
           'о': 'o',
           'п': 'p',
           'р': 'r',
           'с': 's',
           'т': 't',
           'у': 'u',
           'ф': 'f',
           'х': 'h',
           'ц': 'c',
           'ч': 'ch',
           'ш': 'sh',
           'щ': 'sch',
           'ъ': '_',
           'ы': 'y',
           'ь': '_',
           'э': 'e',
           'ю': 'yu',
           'я': 'ya',
           '\n': ''}
    for c in dic:
        s = s.replace(c, dic[c])
    return s


def csv_create(lines, groupname):
    """Return list for csv of moodle-format"""
    lines_csv = ['username;password;lastname;firstname;middlename;email']
    for str_csv in lines:
        fullname = str_csv.split()
        username = '{}-{}'.format(groupname, translit(fullname[0].lower()))
#        try fullname[3]:
#            line = [username, ""] + fullname
#        else:
        email = username + '@prc.local'
#        email = fullname[3]
        password = pwgen.pwgen(8, symbols=False)
        line = [username, password] + fullname + [email]
#        line = [username, password] + fullname
        lines_csv.append(';'.join(line))

    return lines_csv


def main():
    parser = OptionParser(prog="moodle_logins.py",
                          usage="%prog FILE")
    _, args = parser.parse_args()
    if len(args) != 1:
        parser.print_help()
        exit()

    f = open(args[0], "r")
    f_csv = open(args[0] + '.csv', "w")
    for str_csv in csv_create(f.readlines(), args[0].split('.')[0]):
        f_csv.write(str_csv + '\n')

    f.close()
    f_csv.close()


if __name__ == '__main__':
    main()
